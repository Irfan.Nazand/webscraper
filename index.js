const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    
    let fotballUrl = 'https://www.premierleague.com/clubs/12/Manchester-United/fixtures';

    await page.goto(fotballUrl, {waitUntil: 'networkidle0'});

    const data = await page.evaluate(async() => {

        let logo = Array.from(document.querySelectorAll('section.fixtures img'))
        .map((logo) => logo.src.trim())
        let teams = Array.from(document.querySelectorAll('section.fixtures span.team'))
        .map((partner) => partner.innerText.trim())
        
        return {
            logo,
            teams
        }
    });

    console.log(data)
    await browser.close();
})();